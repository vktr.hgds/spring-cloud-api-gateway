package com.secondservice.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/second-service")
public class Service2Controller {
	
	@GetMapping("/message")
	public String showMessage() {
		return "Welcome from Service-2";
	}
}
