package com.apigateway.demo;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringCloudConfig {

    @Bean
    public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(r -> r.path("/firstService/**")
                        .uri("http://localhost:8081/")
                        .id("firstServiceModule"))

                .route(r -> r.path("/secondService/**")
                        .uri("http://localhost:8082/")
                        .id("secondServiceModule"))
                .build();
    }

}