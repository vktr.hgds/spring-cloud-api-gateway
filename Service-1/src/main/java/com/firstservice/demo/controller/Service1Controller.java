package com.firstservice.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/first-service")
public class Service1Controller {
	
	@GetMapping("/message")
	public String showMessage() {
		return "Welcome from Service-1";
	}
}
